=======
Credits
=======

Development Lead
----------------

.. {# pkglts, doc.authors

* Guillaume Cerutti, <guillaume.cerutti@ens-lyon.fr>

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* Guillaume Cerutti <guillaume.cerutti@inria.fr>

.. #}
