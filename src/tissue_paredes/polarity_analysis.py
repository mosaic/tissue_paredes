import numpy as np
import scipy.ndimage as nd

from cellcomplex.property_topomesh.creation import vertex_topomesh
from cellcomplex.utils import array_dict

from tissue_paredes.wall_analysis import estimate_cell_centers, estimate_cell_scalar_property



def compute_wall_signal_polarities(all_wall_topomesh, signal_name):
    """

    Parameters
    ----------
    all_wall_topomesh
    signal_name

    Returns
    -------

    """

    wall_normals = all_wall_topomesh.wisp_property('normal',3).values()
    signal_orientations = all_wall_topomesh.wisp_property(signal_name+'_polarity',3).values()

    wall_polarity_vectors = dict(zip(all_wall_topomesh.wisps(3),signal_orientations[:,np.newaxis]*wall_normals))
    all_wall_topomesh.update_wisp_property(signal_name+"_polarity_vector",3,wall_polarity_vectors)


def wall_polarity_weights(all_wall_topomesh, signal_name):
    """

    Parameters
    ----------
    all_wall_topomesh
    signal_name

    Returns
    -------

    """

    wall_areas = all_wall_topomesh.wisp_property('area', 3).values()
    # signal_intensities = all_wall_topomesh.wisp_property(signal_name, 3).values()
    signal_intensities_left = all_wall_topomesh.wisp_property('left_' + signal_name, 3).values()
    signal_intensities_right = all_wall_topomesh.wisp_property('right_' + signal_name, 3).values()

    weights = np.ones_like(wall_areas).astype(float)
    weights *= wall_areas
    # weights *= signal_intensities
    weights *= (signal_intensities_left - signal_intensities_right)
    # weights *= (signal_intensities_left + signal_intensities_right)/2.
    weights[np.isnan(weights)] = 0.

    return array_dict(dict(zip(all_wall_topomesh.wisps(3),weights)))


def cell_signal_polarity_topomesh(all_wall_topomesh, signal_names, cell_topomesh=None):
    """

    Parameters
    ----------
    all_wall_topomesh
    signal_name

    Returns
    -------

    """

    wall_cells = all_wall_topomesh.wisp_property('cell_labels', 3).values()

    for signal_name in signal_names:
        if not all_wall_topomesh.has_wisp_property(signal_name+"_polarity_vector",3):
            compute_wall_signal_polarities(all_wall_topomesh, signal_name)

    if cell_topomesh is None:
        cell_centers = estimate_cell_centers(all_wall_topomesh)
        cell_labels = list(cell_centers.keys())
        cell_topomesh = vertex_topomesh(cell_centers)
        cell_topomesh.update_wisp_property("label",0,dict(zip(cell_labels,cell_labels)))
    else:
        cell_labels = cell_topomesh.wisp_property("label").values()

    cell_signals = {}
    cell_polarity_vectors = {}
    for signal_name in signal_names:
        weights = np.abs(wall_polarity_weights(all_wall_topomesh,signal_name).values(list(all_wall_topomesh.wisps(3))))
        wall_areas = all_wall_topomesh.wisp_property("area",3).values()
        wall_polarity_vectors = all_wall_topomesh.wisp_property(signal_name+"_polarity_vector",3).values()
        wall_polarity_vectors[np.isnan(wall_polarity_vectors)] = 0
        cell_signals[signal_name] = estimate_cell_scalar_property(all_wall_topomesh,signal_name)

        cell_polarity_vectors[signal_name] =  np.transpose([nd.sum(weights*wall_polarity_vectors[:,k], wall_cells[:,0], index=cell_labels) for k in range(3)])
        cell_polarity_vectors[signal_name] += np.transpose([nd.sum(weights*wall_polarity_vectors[:,k], wall_cells[:,1], index=cell_labels) for k in range(3)])
        # cell_polarity_vectors[signal_name] = cell_polarity_vectors[signal_name]/(nd.sum(weights, wall_cells[:,0], index=cell_labels) + nd.sum(weights, wall_cells[:,1], index=cell_labels))[:, np.newaxis]
        cell_polarity_vectors[signal_name] = cell_polarity_vectors[signal_name]/(nd.sum(wall_areas, wall_cells[:,0], index=cell_labels) + nd.sum(wall_areas, wall_cells[:,1], index=cell_labels))[:, np.newaxis]

    for signal_name in signal_names:
        cell_topomesh.update_wisp_property(signal_name,0,dict(zip(cell_labels,cell_signals[signal_name].values(cell_labels))))
        cell_topomesh.update_wisp_property(signal_name+"_polarity_vector",0,dict(zip(cell_labels,cell_polarity_vectors[signal_name])))

    return cell_topomesh


def cell_signal_polarity_histograms(all_wall_topomesh, signal_name, n_points=90):
    """

    Parameters
    ----------
    all_wall_topomesh
    signal_name

    Returns
    -------

    """

    wall_cells = all_wall_topomesh.wisp_property('cell_labels', 3).values()

    if not all_wall_topomesh.has_wisp_property(signal_name+"_polarity_vector",3):
        compute_wall_signal_polarities(all_wall_topomesh, signal_name)

    wall_areas = all_wall_topomesh.wisp_property('area', 3).values()

    wall_weights = np.abs(wall_polarity_weights(all_wall_topomesh,signal_name))

    wall_polarity_vectors = all_wall_topomesh.wisp_property(signal_name+"_polarity_vector",3).values()

    cell_labels = np.unique(wall_cells)

    polarity_thetas = np.radians(np.linspace(-180, 179, n_points))
    theta_sigma = np.pi / 6.
    polarity_histograms = dict(zip(np.unique(wall_cells), [np.zeros_like(polarity_thetas) for c in cell_labels]))

    for w, (l,r), polarity_vector in zip(all_wall_topomesh.wisps(3), wall_cells, wall_polarity_vectors):
        if np.linalg.norm(polarity_vector) > 0:
            polarity_theta = np.sign(polarity_vector[1]) * np.arccos(polarity_vector[0] / np.linalg.norm(polarity_vector))
            if (polarity_vector[1] == 0) and (polarity_vector[0] < 0): polarity_theta = -np.pi

            theta_weights = np.sum([np.exp(-np.power(polarity_thetas - polarity_theta + k * np.pi, 2.) / (2 * np.power(theta_sigma, 2.))) for k in [-2, 0, 2]], axis=0) / (theta_sigma * np.sqrt(2 * np.pi))
            polarity_histograms[l] += wall_weights[w] * theta_weights
            polarity_histograms[r] += wall_weights[w] * theta_weights

    cell_wall_weights = dict(zip(cell_labels, nd.sum(wall_weights, wall_cells[:,0], index=cell_labels) + nd.sum(wall_weights, wall_cells[:,1], index=cell_labels)))
    cell_wall_areas = dict(zip(cell_labels, nd.sum(wall_areas, wall_cells[:,0], index=cell_labels) + nd.sum(wall_areas, wall_cells[:,1], index=cell_labels)))
    for c in cell_labels:
        polarity_histograms[c] = polarity_histograms[c] / cell_wall_weights[c]
        # polarity_histograms[c] = polarity_histograms[c] / cell_wall_areas[c]

    return polarity_histograms, polarity_thetas
