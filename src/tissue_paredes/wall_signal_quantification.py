# -*- python -*-
# -*- coding: utf-8 -*-
#
#       tissue_paredes.wall_signal_quantification
#
#       File author(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File maintainer(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
# ------------------------------------------------------------------------------

import logging
from copy import deepcopy
from time import time as current_time
from multiprocessing.pool import ThreadPool as Pool
import gc

import numpy as np
import pandas as pd
from scipy.ndimage.filters import gaussian_filter
from scipy.optimize import curve_fit
from scipy.stats import norm as normal_distribution
from scipy.stats import f_oneway


from tissue_paredes.wall_analysis import compute_wall_property, estimate_cell_centers
from tissue_paredes.utils.size_tools import getsize

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_cell_property_from_faces, compute_topomesh_vertex_property_from_faces
from cellcomplex.property_topomesh.extraction import cell_topomesh
from cellcomplex.utils import array_dict


def quantify_wall_signals(all_wall_topomesh, img_dict, channel_names=None, wall_sigma=0.6):
    """

    Parameters
    ----------
    all_wall_topomesh
    img_dict
    channel_names
    wall_sigma

    Returns
    -------

    """

    if channel_names is None:
        channel_names = img_dict.keys()
    channel_names = [c for c in channel_names if c in img_dict.keys()]


    if not all_wall_topomesh.has_wisp_property('vertices',2,is_computed=True):
        compute_topomesh_property(all_wall_topomesh, 'vertices', 2)

    voxelsize = np.array(list(img_dict.values())[0].voxelsize)
    coords = np.array(all_wall_topomesh.wisp_property('barycenter',0).values()/voxelsize,int)


    filtered_img_dict = {}
    for channel_name in channel_names:
        filtered_img_dict[channel_name] = gaussian_filter(img_dict[channel_name].get_array().astype(float),sigma=wall_sigma/voxelsize,order=0)

        points_signal = filtered_img_dict[channel_name][tuple([coords[:,0],coords[:,1],coords[:,2]])]
        all_wall_topomesh.update_wisp_property(channel_name,0,points_signal)

        face_signal = all_wall_topomesh.wisp_property(channel_name, 0).values(all_wall_topomesh.wisp_property('vertices', 2).values()).mean(axis=1)
        all_wall_topomesh.update_wisp_property(channel_name, 2, face_signal)
        compute_topomesh_cell_property_from_faces(all_wall_topomesh, channel_name, weighting='area', reduce='mean')

    return


def quantify_wall_membrane_signals(all_wall_topomesh, img_dict, membrane_channel=None, channel_names=None, wall_distance=0.6, exclude_contours=True):
    """

    Parameters
    ----------
    all_wall_topomesh
    img_dict
    membrane_channel
    channel_names
    wall_distance

    Returns
    -------

    """

    if membrane_channel is None:
        membrane_channel = list(img_dict.keys())[0]
    assert membrane_channel in img_dict.keys()

    membrane_img = img_dict[membrane_channel]

    if channel_names is None:
        channel_names = list(img_dict.keys())
    channel_names = [c for c in channel_names if c in img_dict.keys()]

    # compute_wall_property(all_wall_topomesh,'area')
    # compute_wall_property(all_wall_topomesh,'normal',orient_normals=True)

    cell_centers = estimate_cell_centers(all_wall_topomesh)

    def oriented_normal_wall_topomesh(input_topomesh):
        start_time = current_time()

        wall_topomesh = deepcopy(input_topomesh)

        left_label, right_label = wall_topomesh.wisp_property('cell_labels', 3).values()[0]

        logging.info("    --> Wall topomesh ("+str(left_label)+","+str(right_label)+") : " + str(wall_topomesh.nb_wisps(0)) + " Vertices, " + str(wall_topomesh.nb_wisps(2)) + " Triangles")
        logging.info("    --> Wall topomesh (" + str(left_label) + "," + str(right_label) + ") : "+str(np.round(getsize(wall_topomesh)/(1024*1024.),1))+" Mo")

        compute_topomesh_property(wall_topomesh,'borders',1)
        compute_topomesh_property(wall_topomesh,'borders',2)
        compute_topomesh_property(wall_topomesh,'vertices',2)
        compute_wall_property(wall_topomesh,'normal',orient_normals=True,cell_centers=cell_centers)
        compute_topomesh_vertex_property_from_faces(wall_topomesh,'normal',neighborhood=5,adjacency_sigma=3.)

        if exclude_contours:
            contour_edges = [e for e in wall_topomesh.wisps(1) if wall_topomesh.nb_regions(1,e)<2]
            wall_topomesh.update_wisp_property('contour',1,array_dict([e in contour_edges for e in wall_topomesh.wisps(1)],keys=list(wall_topomesh.wisps(1))))
            contour_vertices = [v for v in wall_topomesh.wisps(0) if np.any([e in contour_edges for e in wall_topomesh.regions(0,v)])]
            wall_topomesh.update_wisp_property('contour',0,array_dict([v in contour_vertices for v in wall_topomesh.wisps(0)],keys=list(wall_topomesh.wisps(0))))

        logging.info("    --> Wall topomesh (" + str(left_label) + "," + str(right_label) + ") : Computed normals ")
        logging.info("    --> Wall topomesh (" + str(left_label) + "," + str(right_label) + ") : " + str(np.round(getsize(wall_topomesh) / (1024 * 1024.), 1)) + " Mo")

        logging.getLogger().setLevel(logging.INFO)
        logging.info("    --> Vertex properties : " + str(list(wall_topomesh.wisp_property_names(0))))
        logging.info("    --> Edge properties : " + str(list(wall_topomesh.wisp_property_names(1))))
        logging.info("    --> Triangle properties : " + str(list(wall_topomesh.wisp_property_names(2))))
        logging.info("    --> Wall properties : " + str(list(wall_topomesh.wisp_property_names(3))))

        logging.info("    --> Computing wall normals ("+str(left_label)+","+str(right_label)+") ["+str(current_time() - start_time)+" s]")

        return wall_topomesh

    wall_topomeshes = [cell_topomesh(all_wall_topomesh, cells=[c], copy_properties=True, preserve_ids=False) for c in all_wall_topomesh.wisps(3)]
    logging.info(str(len(wall_topomeshes))+" Walls for normal computation : "+str(np.round(getsize(wall_topomeshes)/(1024*1024.),1))+" Mo")

    start_time = current_time()
    pool = Pool()
    normal_wall_topomeshes = pool.map(oriented_normal_wall_topomesh, wall_topomeshes)
    del pool
    gc.collect()

    wall_meshes = {}
    wall_areas = {}
    for c,w in zip(all_wall_topomesh.wisps(3),normal_wall_topomeshes):
        left_label, right_label = w.wisp_property('cell_labels', 3).values()[0]
        w.update_wisp_property('label', 3, dict([(i_c, c) for i_c in w.wisps(3)]))
        wall_meshes[(left_label,right_label)] = w
        wall_areas[(left_label,right_label)] = w.wisp_property('area',3).values()[0]

    # all_wall_topomesh.update_wisp_property('normal', 0, dict(zip([v for w in normal_wall_topomeshes for v in w.wisps(0)],[n for w in normal_wall_topomeshes for n in w.wisp_property('normal',0).values(list(w.wisps(0)))])))
    # all_wall_topomesh.update_wisp_property('normal', 0, dict(zip([v for c in all_wall_topomesh.wisps(3) for v in w.wisps(0)], [n for w in normal_wall_topomeshes for n in w.wisp_property('normal', 0).values(list(w.wisps(0)))])))

    all_wall_topomesh.update_wisp_property('normal', 3, dict([(c,w.wisp_property('normal',3).values()[0]) for c,w in zip(all_wall_topomesh.wisps(3),normal_wall_topomeshes)]))

    logging.info("  --> Computing wall normals [" + str(current_time() - start_time) + " s]")

    # wall_meshes = {}
    # for c in all_wall_topomesh.wisps(3):
    #     wall_topomesh = cell_topomesh(all_wall_topomesh, cells=[c], copy_properties=True, preserve_ids=False)
    #     print(wall_topomesh)
    #     compute_wall_property(all_wall_topomesh,'normal',orient_normals=True)
    #     compute_topomesh_vertex_property_from_faces(wall_topomesh,'normal',neighborhood=5.,adjacency_sigma=3.)
    #     wall_meshes[tuple(wall_topomesh.wisp_property('cell_labels', 3).values()[0])] = wall_topomesh
    #
    # wall_areas = {}
    # for i_w, (left_label, right_label) in enumerate(wall_meshes.keys()):
    #
    #     start_time = current_time()
    #     wall_topomesh = wall_meshes[(left_label,right_label)]
    #
    #
    #     wall_areas[(left_label,right_label)] = wall_topomesh.wisp_property('area',3).values()[0]
    #
    #     if exclude_contours:
    #         contour_edges = [e for e in wall_topomesh.wisps(1) if wall_topomesh.nb_regions(1,e)<2]
    #         wall_topomesh.update_wisp_property('contour',1,array_dict([e in contour_edges for e in wall_topomesh.wisps(1)],keys=list(wall_topomesh.wisps(1))))
    #         contour_vertices = [v for v in wall_topomesh.wisps(0) if np.any([e in contour_edges for e in wall_topomesh.regions(0,v)])]
    #         wall_topomesh.update_wisp_property('contour',0,array_dict([v in contour_vertices for v in wall_topomesh.wisps(0)],keys=list(wall_topomesh.wisps(0))))
    #
    #     logging.info("  --> Computing wall normals "+str(i_w+1)+"/"+str(len(wall_meshes))+" ("+str(left_label)+","+str(right_label)+") ["+str(current_time() - start_time)+" s]")


    def normal_function(x, amplitude=1, loc=0, scale=1, offset=0):
        return offset+amplitude*normal_distribution.pdf(x,loc=loc,scale=scale)

    wall_sigma = 1.5
    line_sigma = 1.
    fitting_error_threshold = 10.

    signal_polarities = dict([(channel_name,dict([(c,np.nan) for c in all_wall_topomesh.wisps(3)])) for channel_name in channel_names])
    signal_shifts = dict([(channel_name,dict([(c,np.nan) for c in all_wall_topomesh.wisps(3)])) for channel_name in channel_names])

    signal_vertex_intensities = dict([(channel_name,dict([(v,np.nan) for v in all_wall_topomesh.wisps(0)])) for channel_name in channel_names])
    signal_vertex_intensities_left = dict([(channel_name,dict([(v,np.nan) for v in all_wall_topomesh.wisps(0)])) for channel_name in channel_names])
    signal_vertex_intensities_right = dict([(channel_name,dict([(v,np.nan) for v in all_wall_topomesh.wisps(0)])) for channel_name in channel_names])

    signal_intensities = dict([(channel_name,dict([(c,np.nan) for c in all_wall_topomesh.wisps(3)])) for channel_name in channel_names])
    signal_intensities_left = dict([(channel_name,dict([(c,np.nan) for c in all_wall_topomesh.wisps(3)])) for channel_name in channel_names])
    signal_intensities_right = dict([(channel_name,dict([(c,np.nan) for c in all_wall_topomesh.wisps(3)])) for channel_name in channel_names])


    wall_topomeshes = [wall_meshes[(left_label, right_label)] for i_w, (left_label, right_label) in enumerate(list(wall_meshes.keys()))]
    wall_cell_labels = [(left_label, right_label) for i_w, (left_label, right_label) in enumerate(list(wall_meshes.keys()))]


    def signal_polarity_data(wall_topomesh, img_dict=img_dict, channel_names=channel_names, membrane_channel=membrane_channel):
        start_time = current_time()

        wall_label = wall_topomesh.wisp_property('label',3).values()[0]
        left_label,right_label = wall_topomesh.wisp_property('cell_labels',3).values()[0]
        membrane_img = img_dict[membrane_channel]

        wall_vertex_data = {}
        for channel_name in channel_names:
            wall_vertex_data[channel_name] = dict([(v,np.nan) for v in wall_topomesh.wisps(0)])
            wall_vertex_data[channel_name+"_left"] = dict([(v,np.nan) for v in wall_topomesh.wisps(0)])
            wall_vertex_data[channel_name+"_right"] = dict([(v,np.nan) for v in wall_topomesh.wisps(0)])

        wall_data = {}
        wall_data['label'] = [wall_label]
        wall_data['left_label'] = [left_label]
        wall_data['right_label'] = [right_label]
        for channel_name in channel_names:
            wall_data[channel_name+"_polarity"] = [np.nan]
            wall_data[channel_name] = [np.nan]
            wall_data[channel_name+"_left"] = [np.nan]
            wall_data[channel_name+"_right"] = [np.nan]

        wall_bbox = []
        wall_bbox += [np.min([wall_topomesh.wisp_property('barycenter',0).values()+side*wall_sigma*wall_topomesh.wisp_property('normal',0).values() for side in [-1,1]],axis=(0,1))]
        wall_bbox += [np.max([wall_topomesh.wisp_property('barycenter',0).values()+side*wall_sigma*wall_topomesh.wisp_property('normal',0).values() for side in [-1,1]],axis=(0,1))]

        wall_bbox = np.sort(np.transpose(wall_bbox)/np.array(membrane_img.voxelsize)[:,np.newaxis])
        wall_bbox[:,0] = np.minimum(np.maximum(0,np.floor(wall_bbox[:,0])),np.array(membrane_img.shape)-1)
        wall_bbox[:,1] = np.minimum(np.maximum(0,np.ceil(wall_bbox[:,1])),np.array(membrane_img.shape)-1)

        wall_neighborhood_coords = np.mgrid[wall_bbox[0, 0]:wall_bbox[0, 1] + 1, wall_bbox[1, 0]:wall_bbox[1, 1] + 1, wall_bbox[2, 0]:wall_bbox[2, 1] + 1]
        wall_neighborhood_coords = np.concatenate(np.concatenate(np.transpose(wall_neighborhood_coords, (1, 2, 3, 0))))
        wall_neighborhood_coords = np.unique(wall_neighborhood_coords, axis=0).astype(int)

        wall_neighborhood_points = wall_neighborhood_coords * np.array(membrane_img.voxelsize)

        wall_neighborhood_coords = tuple(np.transpose(wall_neighborhood_coords))

        wall_neighborhood_membrane = membrane_img[wall_neighborhood_coords]

        wall_neighborhood_signal = {}
        for channel_name in channel_names:
            signal_img = img_dict[channel_name]
            wall_neighborhood_signal[channel_name] = signal_img[wall_neighborhood_coords]

        wall_vertices = np.array(list(wall_topomesh.wisps(0)))
        if exclude_contours:
            wall_vertices = wall_vertices[np.logical_not(wall_topomesh.wisp_property('contour', 0).values())]
        wall_points = wall_topomesh.wisp_property('barycenter', 0).values(wall_vertices)
        wall_normal_vectors = wall_topomesh.wisp_property('normal', 0).values(wall_vertices)

        if len(wall_points) > 0:

            wall_neighborhood_vectors = wall_neighborhood_points[np.newaxis, :] - wall_points[:, np.newaxis]

            wall_neighborhood_dot_products = np.einsum("...ij,...ij->...i", wall_neighborhood_vectors, wall_normal_vectors[:, np.newaxis])

            wall_neighborhood_projected_points = wall_points[:, np.newaxis] + wall_neighborhood_dot_products[:, :, np.newaxis] * wall_normal_vectors[:, np.newaxis]
            wall_neighborhood_projected_distances = np.linalg.norm(wall_neighborhood_projected_points - wall_neighborhood_points[np.newaxis, :], axis=2)

            membrane_shifts = []

            signal_membrane_shifts = dict((c, []) for c in channel_names)
            signal_amplitudes = dict((c, []) for c in channel_names)
            fitting_errors = dict((c, []) for c in channel_names)

            signal_vertices = dict((c, []) for c in channel_names)
            signal_left_intensities = dict((c, []) for c in channel_names)
            signal_right_intensities = dict((c, []) for c in channel_names)

            logging.info("    --> " + str(len(wall_neighborhood_dot_products)) + " lines to consider")

            for i_line, (wall_distances, line_distances, wall_vertex) in enumerate(zip(wall_neighborhood_dot_products, wall_neighborhood_projected_distances, wall_vertices)):
                # line_weights = (line_distances<3.*line_sigma)&(np.abs(wall_distances)<1.5*wall_sigma)

                # line_weights = (np.exp(-np.power(line_distances,2)/np.power(line_sigma,2)))*(np.abs(wall_distances)<wall_sigma)
                line_weights = (np.exp(-np.power(line_distances, 2) / np.power(line_sigma, 2))) * (np.abs(wall_distances) < wall_sigma)

                # p0 is the initial guess for the fitting coefficients (A, mu and sigma above)
                p0 = [1., 0., 1., 0.]

                try:
                    line_weights = (line_weights > 0.1).astype(float)
                    membrane_p, membrane_cov = curve_fit(normal_function, wall_distances[line_weights > 0.1], wall_neighborhood_membrane[line_weights > 0.1], p0=p0, maxfev=int(1e3))
                    # membrane_p,_ = curve_fit(normal_function, wall_distances[line_weights>0], wall_neighborhood_membrane[line_weights>0], p0=p0, sigma=line_weights[line_weights>0])
                    membrane_amplitude, membrane_loc, membrane_scale, membrane_offset = membrane_p

                    membrane_shifts += [membrane_loc]

                except Exception as e:
                    pass
                else:
                    for channel_name in channel_names:
                        try:
                            signal_p, signal_cov = curve_fit(normal_function, wall_distances[line_weights > 0.1], wall_neighborhood_signal[channel_name][line_weights > 0.1], p0=p0, maxfev=int(1e3))
                            signal_amplitude, signal_loc, signal_scale, signal_offset = signal_p
                        except Exception as e:
                            pass
                        else:
                            signal_membrane_shifts[channel_name] += [signal_loc - membrane_loc]
                            signal_amplitudes[channel_name] += [normal_function(signal_loc, *signal_p)]

                            # np.power(normal_function(wall_distances[line_weights>0.1],*membrane_p)-wall_neighborhood_membrane[line_weights>0.1],2).mean()
                            membrane_relative_error = float((np.abs(normal_function(wall_distances[line_weights > 0.1], *membrane_p) - wall_neighborhood_membrane[line_weights > 0.1]) / normal_function(
                                wall_distances[line_weights > 0.1], *membrane_p)).mean())
                            signal_relative_error = float((np.abs(
                                normal_function(wall_distances[line_weights > 0.1], *signal_p) - wall_neighborhood_signal[channel_name][line_weights > 0.1]) / normal_function(
                                wall_distances[line_weights > 0.1], *signal_p)).mean())
                            fitting_errors[channel_name] += [(membrane_relative_error, signal_relative_error)]
                            # logging.info("      --> "+str(i_line+1)+"/"+str(len(wall_neighborhood_dot_products))+" OK "+str(fitting_errors[-1]))

                            membrane_wall_distances = wall_distances - membrane_loc
                            left_signal = wall_neighborhood_signal[channel_name][(line_weights > 0.1) & (membrane_wall_distances >= 0) & (np.abs(membrane_wall_distances) < wall_distance)].mean()
                            right_signal = wall_neighborhood_signal[channel_name][(line_weights > 0.1) & (membrane_wall_distances <= 0) & (np.abs(membrane_wall_distances) < wall_distance)].mean()

                            signal_vertices[channel_name] += [wall_vertex]
                            signal_left_intensities[channel_name] += [left_signal]
                            signal_right_intensities[channel_name] += [right_signal]

            for channel_name in channel_names:
                # if len(signal_membrane_shifts)>0:
                if len(signal_left_intensities[channel_name]) > 0:
                    signal_vertices[channel_name] = np.array(signal_vertices[channel_name])
                    signal_membrane_shifts[channel_name] = np.array(signal_membrane_shifts[channel_name])
                    signal_amplitudes[channel_name] = np.array(signal_amplitudes[channel_name])
                    signal_left_intensities[channel_name] = np.array(signal_left_intensities[channel_name])
                    signal_right_intensities[channel_name] = np.array(signal_right_intensities[channel_name])

                    membrane_validity = np.ones_like(signal_membrane_shifts[channel_name]).astype(bool)
                    membrane_validity = membrane_validity & (np.array(fitting_errors[channel_name])[:, 0] < fitting_error_threshold)

                    signal_vertices[channel_name] = signal_vertices[channel_name][membrane_validity]
                    signal_membrane_shifts[channel_name] = signal_membrane_shifts[channel_name][membrane_validity]
                    signal_amplitudes[channel_name] = signal_amplitudes[channel_name][membrane_validity]
                    signal_left_intensities[channel_name] = signal_left_intensities[channel_name][membrane_validity]
                    signal_right_intensities[channel_name] = signal_right_intensities[channel_name][membrane_validity]

                    wall_vertex_data[channel_name].update(dict(zip(signal_vertices[channel_name], np.mean([signal_left_intensities[channel_name], signal_right_intensities[channel_name]], axis=0))))
                    wall_vertex_data[channel_name+"_left"].update(dict(zip(signal_vertices[channel_name], signal_left_intensities[channel_name])))
                    wall_vertex_data[channel_name+"_right"].update(dict(zip(signal_vertices[channel_name], signal_right_intensities[channel_name])))

                    if len(signal_left_intensities) > 0:
                        anova = f_oneway(signal_left_intensities[channel_name], signal_right_intensities[channel_name])
                        if anova.pvalue < 1e-3:
                            signal_polarity = -np.sign(np.nanmedian(signal_left_intensities[channel_name] - signal_right_intensities[channel_name]))
                        elif anova.pvalue < 1e-2:
                            signal_polarity = -np.sign(np.nanmedian(signal_left_intensities[channel_name] - signal_right_intensities[channel_name])) / 2.
                        elif anova.pvalue < 5e-2:
                            signal_polarity = -np.sign(np.nanmedian(signal_left_intensities[channel_name] - signal_right_intensities[channel_name])) / 4.
                        else:
                            signal_polarity = 0

                        wall_data[channel_name+"_polarity"] = [signal_polarity]
                        signal_shifts[channel_name][c] = np.nanmedian(signal_membrane_shifts[channel_name])
                        #wall_data[channel_name] = [np.nanmedian(signal_amplitudes[channel_name])]
                        wall_data[channel_name] = [np.nanmean([np.nanmedian(signal_left_intensities[channel_name]), np.nanmedian(signal_right_intensities[channel_name])])]

                        wall_data[channel_name+"_left"] = [np.nanmedian(signal_left_intensities[channel_name])]
                        wall_data[channel_name+"_right"] = [np.nanmedian(signal_right_intensities[channel_name])]

                        logging.info("  <-- Computing local " + channel_name + " polarity (" + str(left_label) + "," + str(right_label) + ") [" + str(current_time() - start_time) + " s]")

        return wall_vertex_data, wall_data

    start_time = current_time()
    pool = Pool()
    all_wall_data = pool.map(signal_polarity_data, wall_topomeshes)
    del pool
    gc.collect()

    wall_vertex_data = [d[0] for d in all_wall_data]
    wall_data = [d[1] for d in all_wall_data]
    logging.info("  --> Computing local polarities [" + str(current_time() - start_time) + " s]")

    wall_df = pd.concat([pd.DataFrame().from_dict(data) for data in wall_data])
    print(wall_df)

    for channel_name in channel_names:
        signal_intensities[channel_name].update(dict(zip(wall_df['label'].values,wall_df[channel_name].values)))
        signal_intensities_left[channel_name].update(dict(zip(wall_df['label'].values,wall_df[channel_name+"_left"].values)))
        signal_intensities_right[channel_name].update(dict(zip(wall_df['label'].values,wall_df[channel_name+"_right"].values)))
        signal_polarities[channel_name].update(dict(zip(wall_df['label'].values,wall_df[channel_name+"_polarity"].values)))

        # for w,vertex_data in zip(wall_topomeshes,wall_vertex_data):
        #     signal_vertex_intensities[channel_name].update(dict(zip(w.wisps(0),[vertex_data[channel_name][i_v] for i_v,v in enumerate(w.wisps(0))])))
        #     signal_vertex_intensities_left[channel_name].update(dict(zip(w.wisps(0),[vertex_data[channel_name+"_left"][i_v] for i_v,v in enumerate(w.wisps(0))])))
        #     signal_vertex_intensities_right[channel_name].update(dict(zip(w.wisps(0),[vertex_data[channel_name+"_right"][i_v] for i_v,v in enumerate(w.wisps(0))])))

    # bbox_sizes = {}
    #
    # for i_w, c in enumerate(all_wall_topomesh.wisps(3)):
    #
    #     start_time = current_time()
    #     left_label,right_label = all_wall_topomesh.wisp_property('cell_labels',3)[c]
    #
    #     logging.info("  --> Computing signals "+str(i_w+1)+"/"+str(all_wall_topomesh.nb_wisps(3))+" ("+str(left_label)+","+str(right_label)+")")
    #
    #     wall_topomesh = wall_meshes[(left_label,right_label)]
    #
    #     wall_bbox = []
    #     wall_bbox += [np.min([wall_topomesh.wisp_property('barycenter',0).values()+side*wall_sigma*wall_topomesh.wisp_property('normal',0).values() for side in [-1,1]],axis=(0,1))]
    #     wall_bbox += [np.max([wall_topomesh.wisp_property('barycenter',0).values()+side*wall_sigma*wall_topomesh.wisp_property('normal',0).values() for side in [-1,1]],axis=(0,1))]
    #
    #     wall_bbox = np.sort(np.transpose(wall_bbox)/np.array(membrane_img.voxelsize)[:,np.newaxis])
    #     wall_bbox[:,0] = np.minimum(np.maximum(0,np.floor(wall_bbox[:,0])),np.array(membrane_img.shape)-1)
    #     wall_bbox[:,1] = np.minimum(np.maximum(0,np.ceil(wall_bbox[:,1])),np.array(membrane_img.shape)-1)
    #
    #     bbox_sizes[c] = int(np.prod(wall_bbox[:,1]-wall_bbox[:,0]))
    #
    #     #if bbox_sizes[c]<30000:
    #     if True:
    #
    #         wall_neighborhood_coords = np.mgrid[wall_bbox[0,0]:wall_bbox[0,1]+1,wall_bbox[1,0]:wall_bbox[1,1]+1,wall_bbox[2,0]:wall_bbox[2,1]+1]
    #         wall_neighborhood_coords = np.concatenate(np.concatenate(np.transpose(wall_neighborhood_coords,(1,2,3,0))))
    #         wall_neighborhood_coords = np.unique(wall_neighborhood_coords,axis=0).astype(int)
    #
    #         wall_neighborhood_points = wall_neighborhood_coords*np.array(membrane_img.voxelsize)
    #
    #         wall_neighborhood_coords = tuple(np.transpose(wall_neighborhood_coords))
    #
    #         wall_neighborhood_membrane = membrane_img[wall_neighborhood_coords]
    #
    #         wall_neighborhood_signal = {}
    #         for channel_name in channel_names:
    #             signal_img = img_dict[channel_name]
    #             wall_neighborhood_signal[channel_name] = signal_img[wall_neighborhood_coords]
    #
    #         wall_vertices = np.array(list(wall_topomesh.wisps(0)))
    #         if exclude_contours:
    #             wall_vertices = wall_vertices[np.logical_not(wall_topomesh.wisp_property('contour', 0).values())]
    #         wall_points = wall_topomesh.wisp_property('barycenter',0).values(wall_vertices)
    #         wall_normal_vectors = wall_topomesh.wisp_property('normal',0).values(wall_vertices)
    #
    #         if len(wall_points)>0:
    #
    #             wall_neighborhood_vectors = wall_neighborhood_points[np.newaxis,:] - wall_points[:,np.newaxis]
    #
    #             wall_neighborhood_dot_products = np.einsum("...ij,...ij->...i",wall_neighborhood_vectors,wall_normal_vectors[:,np.newaxis])
    #
    #             wall_neighborhood_projected_points = wall_points[:,np.newaxis] + wall_neighborhood_dot_products[:,:,np.newaxis]*wall_normal_vectors[:,np.newaxis]
    #             wall_neighborhood_projected_distances = np.linalg.norm(wall_neighborhood_projected_points-wall_neighborhood_points[np.newaxis,:],axis=2)
    #
    #             membrane_shifts = []
    #
    #             signal_membrane_shifts = dict((c,[]) for c in channel_names)
    #             signal_amplitudes = dict((c,[]) for c in channel_names)
    #             fitting_errors = dict((c,[]) for c in channel_names)
    #
    #             signal_vertices = dict((c,[]) for c in channel_names)
    #             signal_left_intensities = dict((c,[]) for c in channel_names)
    #             signal_right_intensities = dict((c,[]) for c in channel_names)
    #
    #             logging.info("    --> "+str(len(wall_neighborhood_dot_products))+" lines to consider")
    #
    #             for i_line, (wall_distances, line_distances, wall_vertex) in enumerate(zip(wall_neighborhood_dot_products,wall_neighborhood_projected_distances,wall_vertices)):
    #                 # line_weights = (line_distances<3.*line_sigma)&(np.abs(wall_distances)<1.5*wall_sigma)
    #
    #                 # line_weights = (np.exp(-np.power(line_distances,2)/np.power(line_sigma,2)))*(np.abs(wall_distances)<wall_sigma)
    #                 line_weights = (np.exp(-np.power(line_distances,2)/np.power(line_sigma,2)))*(np.abs(wall_distances)<wall_sigma)
    #
    #                 # p0 is the initial guess for the fitting coefficients (A, mu and sigma above)
    #                 p0 = [1., 0., 1., 0.]
    #
    #                 try:
    #                     line_weights = (line_weights>0.1).astype(float)
    #                     membrane_p,membrane_cov = curve_fit(normal_function, wall_distances[line_weights>0.1], wall_neighborhood_membrane[line_weights>0.1], p0=p0, maxfev=int(1e3))
    #                     # membrane_p,_ = curve_fit(normal_function, wall_distances[line_weights>0], wall_neighborhood_membrane[line_weights>0], p0=p0, sigma=line_weights[line_weights>0])
    #                     membrane_amplitude, membrane_loc, membrane_scale, membrane_offset = membrane_p
    #
    #                     membrane_shifts += [membrane_loc]
    #
    #                 except Exception as e:
    #                     pass
    #                 else:
    #                     for channel_name in channel_names:
    #                         try:
    #                             signal_p,signal_cov = curve_fit(normal_function, wall_distances[line_weights>0.1], wall_neighborhood_signal[channel_name][line_weights>0.1], p0=p0, maxfev=int(1e3))
    #                             signal_amplitude, signal_loc, signal_scale, signal_offset = signal_p
    #                         except Exception as e:
    #                             pass
    #                         else:
    #                             signal_membrane_shifts[channel_name] += [signal_loc-membrane_loc]
    #                             signal_amplitudes[channel_name] += [normal_function(signal_loc,*signal_p)]
    #
    #                             # np.power(normal_function(wall_distances[line_weights>0.1],*membrane_p)-wall_neighborhood_membrane[line_weights>0.1],2).mean()
    #                             membrane_relative_error = float((np.abs(normal_function(wall_distances[line_weights>0.1],*membrane_p)-wall_neighborhood_membrane[line_weights>0.1])/normal_function(wall_distances[line_weights>0.1],*membrane_p)).mean())
    #                             signal_relative_error = float((np.abs(normal_function(wall_distances[line_weights>0.1],*signal_p)-wall_neighborhood_signal[channel_name][line_weights>0.1])/normal_function(wall_distances[line_weights>0.1],*signal_p)).mean())
    #                             fitting_errors[channel_name] += [(membrane_relative_error,signal_relative_error)]
    #                             # logging.info("      --> "+str(i_line+1)+"/"+str(len(wall_neighborhood_dot_products))+" OK "+str(fitting_errors[-1]))
    #
    #                             membrane_wall_distances = wall_distances-membrane_loc
    #                             left_signal = wall_neighborhood_signal[channel_name][(line_weights>0.1)&(membrane_wall_distances>=0)&(np.abs(membrane_wall_distances)< wall_distance)].mean()
    #                             right_signal = wall_neighborhood_signal[channel_name][(line_weights>0.1)&(membrane_wall_distances<=0)&(np.abs(membrane_wall_distances)< wall_distance)].mean()
    #
    #                             signal_vertices[channel_name] += [wall_vertex]
    #                             signal_left_intensities[channel_name] += [left_signal]
    #                             signal_right_intensities[channel_name] += [right_signal]
    #
    #             for channel_name in channel_names:
    #                 #if len(signal_membrane_shifts)>0:
    #                 if len(signal_left_intensities[channel_name])>0:
    #                     signal_vertices[channel_name] = np.array(signal_vertices[channel_name])
    #                     signal_membrane_shifts[channel_name] = np.array(signal_membrane_shifts[channel_name])
    #                     signal_amplitudes[channel_name] = np.array(signal_amplitudes[channel_name])
    #                     signal_left_intensities[channel_name] = np.array(signal_left_intensities[channel_name])
    #                     signal_right_intensities[channel_name] = np.array(signal_right_intensities[channel_name])
    #
    #                     membrane_validity = np.ones_like(signal_membrane_shifts[channel_name]).astype(bool)
    #                     membrane_validity = membrane_validity & (np.array(fitting_errors[channel_name])[:,0]<fitting_error_threshold)
    #
    #                     signal_vertices[channel_name] = signal_vertices[channel_name][membrane_validity]
    #                     signal_membrane_shifts[channel_name] = signal_membrane_shifts[channel_name][membrane_validity]
    #                     signal_amplitudes[channel_name] = signal_amplitudes[channel_name][membrane_validity]
    #                     signal_left_intensities[channel_name] = signal_left_intensities[channel_name][membrane_validity]
    #                     signal_right_intensities[channel_name] = signal_right_intensities[channel_name][membrane_validity]
    #
    #                     signal_vertex_intensities[channel_name].update(dict(zip(signal_vertices[channel_name],np.mean([signal_left_intensities[channel_name],signal_right_intensities[channel_name]],axis=0))))
    #                     signal_vertex_intensities_left[channel_name].update(dict(zip(signal_vertices[channel_name], signal_left_intensities[channel_name])))
    #                     signal_vertex_intensities_right[channel_name].update(dict(zip(signal_vertices[channel_name], signal_right_intensities[channel_name])))
    #
    #                     if len(signal_left_intensities)>0:
    #                         anova = f_oneway(signal_left_intensities[channel_name],signal_right_intensities[channel_name])
    #                         if anova.pvalue < 1e-3:
    #                             signal_polarity = -np.sign(np.nanmedian(signal_left_intensities[channel_name]-signal_right_intensities[channel_name]))
    #                         elif anova.pvalue < 1e-2:
    #                             signal_polarity = -np.sign(np.nanmedian(signal_left_intensities[channel_name]-signal_right_intensities[channel_name]))/2.
    #                         elif anova.pvalue < 5e-2:
    #                             signal_polarity = -np.sign(np.nanmedian(signal_left_intensities[channel_name]-signal_right_intensities[channel_name]))/4.
    #                         else:
    #                             signal_polarity = 0
    #
    #                         signal_polarities[channel_name][c] = signal_polarity
    #                         signal_shifts[channel_name][c] = np.nanmedian(signal_membrane_shifts[channel_name])
    #                         # signal_intensities[channel_name][c] = np.nanmedian(signal_amplitudes[channel_name])
    #                         signal_intensities[channel_name][c] = np.nanmean([np.nanmedian(signal_left_intensities[channel_name]),np.nanmedian(signal_right_intensities[channel_name])])
    #
    #                         signal_intensities_left[channel_name][c] = np.nanmedian(signal_left_intensities[channel_name])
    #                         signal_intensities_right[channel_name][c] = np.nanmedian(signal_right_intensities[channel_name])
    #
    #                         logging.info("  <-- Computing local "+channel_name+" polarity ("+str(left_label)+","+str(right_label)+") ["+str(current_time() - start_time)+" s]")

    for channel_name in channel_names:
        # all_wall_topomesh.update_wisp_property(channel_name,0,signal_vertex_intensities[channel_name])
        # all_wall_topomesh.update_wisp_property("left_"+channel_name,0,signal_vertex_intensities_left[channel_name])
        # all_wall_topomesh.update_wisp_property("right_"+channel_name,0,signal_vertex_intensities_right[channel_name])

        all_wall_topomesh.update_wisp_property(channel_name,3,signal_intensities[channel_name])
        all_wall_topomesh.update_wisp_property("left_"+channel_name,3,signal_intensities_left[channel_name])
        all_wall_topomesh.update_wisp_property("right_"+channel_name,3,signal_intensities_right[channel_name])
        all_wall_topomesh.update_wisp_property(channel_name+"_polarity",3,signal_polarities[channel_name])

    return

