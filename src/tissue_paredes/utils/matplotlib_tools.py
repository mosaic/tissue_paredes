# -*- python -*-
# -*- coding: utf-8 -*-
#
#       tissue_paredes.utils.tissue_image_tools
#
#       File author(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File maintainer(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
# ------------------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.colors import Normalize

from tissue_paredes.wall_analysis import compute_wall_property

def mpl_draw_wall_lines(all_wall_topomesh, figure=None, property_name=None, value_range=None, colormap="viridis", wall_scale=1/16., linewidth=1, alpha=1.):
    """

    Parameters
    ----------
    all_wall_topomesh
    figure
    property_name
    value_range
    colormap
    wall_scale
    linewidth

    Returns
    -------

    """

    if property_name is not None:
        assert all_wall_topomesh.has_wisp_property(property_name,3,is_computed=True)
        property_dict = all_wall_topomesh.wisp_property(property_name,3)
    else:
        property_dict = dict(zip(all_wall_topomesh.wisps(3),all_wall_topomesh.wisps(3)))

    if figure is None:
        figure = plt.figure()

    if value_range is None:
        value_range = (np.nanmin(list(property_dict.values())),np.nanmax(list(property_dict.values())))

    if not all_wall_topomesh.has_wisp_property('area',3,is_computed=True):
        compute_wall_property(all_wall_topomesh,'area')
    if not all_wall_topomesh.has_wisp_property('normal',3,is_computed=True):
        compute_wall_property(all_wall_topomesh,'normal')
    if not all_wall_topomesh.has_wisp_property('barycenter',3,is_computed=True):
        compute_wall_property(all_wall_topomesh,'barycenter')

    for c in all_wall_topomesh.wisps(3):
        wall_property = property_dict[c]
        wall_area = all_wall_topomesh.wisp_property('area', 3)[c]
        wall_normal = all_wall_topomesh.wisp_property('normal', 3)[c]
        wall_center = all_wall_topomesh.wisp_property('barycenter', 3)[c]
        property_color = mpl.cm.ScalarMappable(cmap=colormap, norm=Normalize(vmin=value_range[0], vmax=value_range[1])).to_rgba(wall_property)
        ortho_normal = wall_scale * np.power(wall_area, 1) * wall_normal[:2][::-1] * np.array([-1, 1])
        figure.gca().plot(wall_center[0] + np.array([-ortho_normal[0], ortho_normal[0]]), wall_center[1] + np.array([-ortho_normal[1], ortho_normal[1]]), color=property_color, linewidth=linewidth, alpha=alpha, zorder=8)

    return figure


def mpl_draw_wall_signal_polarities(all_wall_topomesh, signal_name, figure=None, color='forestgreen', scale_factor=1.):
    """

    Parameters
    ----------
    all_wall_topomesh
    signal_name
    figure
    color

    Returns
    -------

    """

    new_figure = False
    if figure is None:
        figure = plt.figure()
        new_figure = True

    if not all_wall_topomesh.has_wisp_property('barycenter', 3, is_computed=True):
        compute_wall_property(all_wall_topomesh, 'barycenter')

    for w in all_wall_topomesh.wisps(3):
        wall_center = all_wall_topomesh.wisp_property('barycenter', 3)[w]
        wall_normal = all_wall_topomesh.wisp_property('normal', 3)[w]
        wall_polarity = all_wall_topomesh.wisp_property(signal_name + "_polarity", 3)[w]
        wall_polarity_vector = scale_factor*wall_polarity * wall_normal

        polarity = np.abs(wall_polarity) if np.abs(wall_polarity)!=0.25 else 0.5

        if polarity > 0:
            figure.gca().arrow(wall_center[0] - 0.75*wall_polarity_vector[0], wall_center[1] - 0.75*wall_polarity_vector[1], wall_polarity_vector[0], wall_polarity_vector[1],
                               head_width=np.sqrt(scale_factor)*polarity, head_length=np.sqrt(scale_factor)*polarity, edgecolor='k', facecolor=color, linewidth=0.5, alpha=1)
        else:
            figure.gca().scatter(wall_center[0], wall_center[1], s=40, edgecolor='k', facecolor=color, linewidth=0.5, alpha=1)

    if new_figure:
        wall_centers = all_wall_topomesh.wisp_property('barycenter', 3).values()
        figure.gca().set_xlim(np.nanmin(wall_centers[:, 0]), np.nanmax(wall_centers[:, 0]))
        figure.gca().set_ylim(np.nanmin(wall_centers[:, 1]), np.nanmax(wall_centers[:, 1]))

    return figure


def mpl_draw_wall_signal_polarity_vectors(all_wall_topomesh, signal_name, figure=None, scale_factor=1., head_size=3., head_ratio=0.6667, color='g', colormap='viridis'):
    """

    Parameters
    ----------
    all_wall_topomesh
    signal_name
    figure
    scale_factor
    head_size
    head_ratio
    color

    Returns
    -------

    """

    new_figure = False
    if figure is None:
        figure = plt.figure()
        new_figure = True


    if not all_wall_topomesh.has_wisp_property('barycenter',3,is_computed=True):
        compute_wall_property(all_wall_topomesh,'barycenter')
        
    for w in all_wall_topomesh.wisps(3):
        wall_center = all_wall_topomesh.wisp_property('barycenter',3)[w]
        wall_normal = all_wall_topomesh.wisp_property('normal',3)[w]
        wall_polarity = all_wall_topomesh.wisp_property(signal_name+"_polarity",3)[w]
        wall_polarity_vector = all_wall_topomesh.wisp_property(signal_name+"_polarity_vector",3)[w]
        signal_intensity_left = all_wall_topomesh.wisp_property('left_' + signal_name, 3)[w]
        signal_intensity_right = all_wall_topomesh.wisp_property('right_' + signal_name, 3)[w]

        # intensity = signal_intensity_left - signal_intensity_right
        # intensity = np.sign(wall_polarity)
        intensity = np.linalg.norm(wall_polarity_vector)

        wall_polarity_color = mpl.cm.ScalarMappable(cmap=colormap,norm=Normalize(vmin=-0.5,vmax=1.5)).to_rgba(np.abs(wall_polarity))

        # wall_polarity_vector = scale_factor * wall_polarity_vector / np.linalg.norm(wall_polarity_vector)
        # wall_polarity_vector = scale_factor * intensity * wall_normal
        wall_polarity_vector = scale_factor * wall_polarity_vector

        if np.linalg.norm(wall_polarity_vector) > 0:
            figure.gca().arrow(wall_center[0] - 0.75 * wall_polarity_vector[0], wall_center[1] - 0.75 * wall_polarity_vector[1], wall_polarity_vector[0], wall_polarity_vector[1], head_width=np.sqrt(np.abs(intensity) * scale_factor),
                               head_length=np.sqrt(np.abs(intensity) * scale_factor), edgecolor='k', facecolor=wall_polarity_color, linewidth=3, alpha=1)

        # figure.gca().arrow(wall_center[0] - 0.75 * wall_polarity_vector[0], wall_center[1] - 0.75 * wall_polarity_vector[1], wall_polarity_vector[0], wall_polarity_vector[1], width=0.,
        #                    head_width=head_ratio * head_size, head_length=head_size, edgecolor='k', facecolor=color, overhang=0.3, head_starts_at_zero=False, linewidth=0, alpha=1, zorder=9)
        # 
        # for a in range(1, 5):
        #     figure.gca().arrow(wall_center[0] - (0.75 - a * 0.25) * wall_polarity_vector[0], wall_center[1] - (0.75 - a * 0.25) * wall_polarity_vector[1], wall_polarity_vector[0], wall_polarity_vector[1],
        #                        width=0., head_width=(1 - a / 5.) * head_ratio * head_size, head_length=(1 - a / 5.) * head_size, edgecolor='none', facecolor='w', overhang=0.3, head_starts_at_zero=False, linewidth=1, alpha=0.33, zorder=9)

    if new_figure:
        wall_centers = all_wall_topomesh.wisp_property('barycenter',3).values()
        figure.gca().set_xlim(np.nanmin(wall_centers[:,0]),np.nanmax(wall_centers[:,0]))
        figure.gca().set_ylim(np.nanmin(wall_centers[:,1]),np.nanmax(wall_centers[:,1]))

    return figure


def mpl_plot_wall_orthonormals(wall_data, figure=None, property_name=None, value_range=None, colormap="viridis", color='k', linewidth=1, wall_scale=1/10., position_name='center', normal_name='normal'):

    new_figure = False
    if figure is None:
        figure = plt.figure()
        new_figure = True

    wall_centers = {}
    wall_areas = {}
    wall_normals = {}
    for l, r, x, y, z, a, n_x, n_y, n_z in wall_data[['left_label', 'right_label', position_name+'_x', position_name+'_y', position_name+'_z', 'area', normal_name+'_x', normal_name+'_y', normal_name+'_z']].values:
        wall_centers[(l, r)] = np.array([x, y, z])
        wall_areas[(l, r)] = a
        wall_normals[(l, r)] = np.array([n_x, n_y, n_z])

    if property_name is not None and property_name in wall_data.columns:
        wall_property = {}
        for l, r, p in wall_data[['left_label', 'right_label', property_name]].values:
            wall_property[(l, r)] = p
        
        if value_range is None:
            value_range = (np.nanmin(list(wall_property.values())),np.nanmax(list(wall_property.values())))
    else:
        wall_property = None

    for left_label, right_label in wall_centers.keys():
        ortho_normal = wall_scale*np.power(wall_areas[(left_label,right_label)],1)*np.array(wall_normals[(left_label,right_label)])[:2][::-1]*np.array([-1,1])
        center = wall_centers[(left_label,right_label)]
        if wall_property is not None:
            property_color = mpl.cm.ScalarMappable(cmap=colormap, norm=Normalize(vmin=value_range[0], vmax=value_range[1])).to_rgba(wall_property[(left_label,right_label)])
            figure.gca().plot(center[0]+np.array([-ortho_normal[0],ortho_normal[0]]),center[1]+np.array([-ortho_normal[1],ortho_normal[1]]),color=property_color,linewidth=linewidth,zorder=0)
        else:
            figure.gca().plot(center[0]+np.array([-ortho_normal[0],ortho_normal[0]]),center[1]+np.array([-ortho_normal[1],ortho_normal[1]]),color=color,linewidth=linewidth,zorder=0)
        
    if new_figure:
        wall_points = np.array(list(wall_centers.values()))
        figure.gca().set_xlim(np.min(wall_points[:, 0]), np.max(wall_points[:, 0]))
        figure.gca().set_ylim(np.min(wall_points[:, 1]), np.max(wall_points[:, 1]))

    return figure


def plot_cell_polarity_histograms(polarity_histograms, polarity_thetas, cell_centers, figure=None, scale_factor=1/5000., color='g'):
    """

    Parameters
    ----------
    polarity_histograms
    polarity_thetas
    cell_centers
    figure
    scale_factor
    color

    Returns
    -------

    """

    if figure is None:
        figure = plt.figure()

    figure.gca().scatter(np.array(list(cell_centers.values()))[:, 0], np.array(list(cell_centers.values()))[:, 1], s=10, color=color)

    for c in cell_centers.keys():
        histo_x = cell_centers[c][0] + scale_factor * polarity_histograms[c] * np.cos(polarity_thetas)
        histo_y = cell_centers[c][1] + scale_factor * polarity_histograms[c] * np.sin(polarity_thetas)
        figure.gca().plot(list(histo_x) + [histo_x[0]], list(histo_y) + [histo_y[0]], color=color, linewidth=2)

    return figure