# -*- python -*-
# -*- coding: utf-8 -*-
#
#       tissue_paredes.utils.tissue_image_tools
#
#       File author(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File maintainer(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
# ------------------------------------------------------------------------------


import numpy as np
from scipy.ndimage.filters import gaussian_filter

from timagetk.components import SpatialImage, LabelledImage

def square_tissue_image(voxelsize=(0.25,0.25,0.5), size=10):
    """Create a simple labelled image representing a tissue

    Parameters
    ----------
    voxelsize

    Returns
    -------

    """

    shape = np.round(size*np.ones(3)/np.array(voxelsize)).astype(int)

    array = np.ones(tuple(shape),np.uint8)

    array[:shape[0]//2,   :shape[1]//3, shape[2]//2:] = 2
    array[shape[0]//2:, :2*shape[1]//3, shape[2]//2:] = 3
    array[shape[0]//2:, 2*shape[1]//3:, shape[2]//2:] = 4
    array[:shape[0]//2,   shape[1]//3:, shape[2]//2:] = 5

    return LabelledImage(array, voxelsize=voxelsize, no_label_id=1)


def pseudo_gradient_norm(img, wall_sigma=0.5, dtype=np.uint8, intensity=100, count=True):
    """

    Parameters
    ----------
    img
    sigma

    Returns
    -------

    """

    shape = np.array(img.shape)
    voxelsize = np.array(img.voxelsize)

    neighborhood_img = []
    for x in np.arange(-1,2):
        for y in np.arange(-1,2):
            for z in np.arange(-1,2):
                neighborhood_img.append(img.get_array()[1+x:shape[0]-1+x,1+y:shape[1]-1+y,1+z:shape[2]-1+z])
    neighborhood_img = np.sort(np.transpose(neighborhood_img,(1,2,3,0))).reshape((shape-2).prod(),27)

    if count:
        neighborhoods = np.array(list(map(np.unique,neighborhood_img)))
        neighborhood_size = np.array(list(map(len,neighborhoods))).reshape(tuple(list(shape-2)))
    else:
        neighborhood_size = 2-np.all(neighborhood_img == neighborhood_img[...,:1],axis=-1).astype(int)
        neighborhood_size = neighborhood_size.reshape(tuple(list(shape-2)))

    gradient_img = np.zeros_like(img.get_array()).astype(float)
    gradient_img[1:shape[0]-1,1:shape[1]-1,1:shape[2]-1] += (neighborhood_size-1)
    gradient_img = np.pi*intensity*gaussian_filter(gradient_img,sigma=wall_sigma/voxelsize)
    gradient_img = np.minimum(gradient_img,np.iinfo(dtype).max)

    return SpatialImage(gradient_img.astype(dtype),voxelsize=voxelsize)