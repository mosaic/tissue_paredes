import numpy as np
import scipy.ndimage as nd
from scipy.cluster.vq import vq

from tissue_nukem_3d.epidermal_maps import compute_local_2d_signal, nuclei_density_function


def polarity_vector_maps(cell_topomesh, signal_names, compute_divergence=True, xx=None, yy=None, cell_radius=7.5, density_k=0.55):
    """

    Parameters
    ----------
    cell_topomesh
    signal_names
    compute_divergence
    xx
    yy
    cell_radius
    density_k

    Returns
    -------

    """

    for signal_name in signal_names:
        assert cell_topomesh.has_wisp_property(signal_name+"_polarity_vector",0)

    X,Y,Z = np.transpose(cell_topomesh.wisp_property("barycenter",0).values())

    if xx is None or yy is None:
        xx,yy = np.meshgrid(np.linspace(X.min(),X.max(),101),np.linspace(Y.min(),Y.max(),101))

    signal_maps = {}
    signal_data = {}

    nuclei_density = nuclei_density_function(dict(zip(range(len(X)), np.transpose([X, Y, np.zeros_like(X)]))), cell_radius=cell_radius, k=density_k)(xx, yy, np.zeros_like(xx))
    confidence_map = nuclei_density + np.maximum(1 - np.linalg.norm([xx, yy], axis=0) / 60., 0)
    confidence_map = nd.gaussian_filter(confidence_map, sigma=1.0)
    signal_maps['cell_density'] = confidence_map

    for signal_name in signal_names:
        signal_values = cell_topomesh.wisp_property(signal_name,0).values()
        signal_polarity_vectors = cell_topomesh.wisp_property(signal_name+"_polarity_vector",0).values()

        signal_maps[signal_name] = compute_local_2d_signal(np.transpose([X, Y]), np.transpose([xx, yy], (1, 2, 0)),signal_values, cell_radius=cell_radius, density_k=density_k)
        signal_maps[signal_name+'_polarity_vector_x'] = compute_local_2d_signal(np.transpose([X, Y]), np.transpose([xx, yy], (1, 2, 0)), signal_polarity_vectors[:,0], cell_radius=cell_radius, density_k=density_k)
        signal_maps[signal_name+'_polarity_vector_y'] = compute_local_2d_signal(np.transpose([X, Y]), np.transpose([xx, yy], (1, 2, 0)), signal_polarity_vectors[:,1], cell_radius=cell_radius, density_k=density_k)
        signal_maps[signal_name+'_polarity_vector_z'] = compute_local_2d_signal(np.transpose([X, Y]), np.transpose([xx, yy], (1, 2, 0)), signal_polarity_vectors[:,2], cell_radius=cell_radius, density_k=density_k)
        signal_maps[signal_name+'_polarity_vector_norm'] = np.linalg.norm([signal_maps[signal_name+'_polarity_vector_'+dim] for dim in ['x','y','z']], axis=0)

        if compute_divergence:
            dx = xx[1, 1] - xx[0, 0]
            signal_maps[signal_name+'_divergence'] = np.zeros_like(signal_maps[signal_name+'_polarity_vector_x'])
            signal_maps[signal_name+'_divergence'] += nd.gaussian_filter1d(signal_maps[signal_name+'_polarity_vector_x'], sigma=0.5*cell_radius/dx, axis=1, order=1)
            signal_maps[signal_name+'_divergence'] += nd.gaussian_filter1d(signal_maps[signal_name+'_polarity_vector_y'], sigma=0.5*cell_radius/dx, axis=0, order=1)

            signal_maps[signal_name+'_normalized_divergence'] = np.zeros_like(signal_maps[signal_name+'_polarity_vector_x'])
            signal_maps[signal_name+'_normalized_divergence'] += nd.gaussian_filter1d(signal_maps[signal_name+'_polarity_vector_x']/(signal_maps[signal_name+'_polarity_vector_norm']+1e-5),sigma=0.5*cell_radius/dx,axis=1,order=1)
            signal_maps[signal_name+'_normalized_divergence'] += nd.gaussian_filter1d(signal_maps[signal_name+'_polarity_vector_y']/(signal_maps[signal_name+'_polarity_vector_norm']+1e-5),sigma=0.5*cell_radius/dx,axis=0,order=1)

            signal_data[signal_name+'_divergence'] = np.concatenate(signal_maps[signal_name+'_divergence'])[vq(np.transpose([X, Y]), np.concatenate(np.transpose([xx, yy], (1, 2, 0))))[0]]
            signal_data[signal_name+'_normalized_divergence'] = np.concatenate(signal_maps[signal_name+'_normalized_divergence'])[vq(np.transpose([X, Y]), np.concatenate(np.transpose([xx, yy], (1, 2, 0))))[0]]

            cell_topomesh.update_wisp_property(signal_name+'_divergence',0,dict(zip(cell_topomesh.wisps(0),signal_data[signal_name+'_divergence'])))
            cell_topomesh.update_wisp_property(signal_name+'_normalized_divergence',0,dict(zip(cell_topomesh.wisps(0), signal_data[signal_name + '_normalized_divergence'])))

    return signal_maps


