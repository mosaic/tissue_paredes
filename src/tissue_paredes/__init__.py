"""
Polarity Analysis through Robust Extraction and Density Estimation of Signal at cell-wall level
"""
# {# pkglts, base

from . import version

__version__ = version.__version__

# #}
