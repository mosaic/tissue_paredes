# -*- python -*-
# -*- coding: utf-8 -*-
#
#       tissue_paredes.wall_analysis
#
#       File author(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File maintainer(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
# ------------------------------------------------------------------------------


import logging
from time import time as current_time
from copy import deepcopy
import os

import numpy as np
import scipy.ndimage as nd

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_cell_property_from_faces
from cellcomplex.property_topomesh.extraction import cell_topomesh

from cellcomplex.utils import array_dict

def compute_wall_property(all_wall_topomesh, property_name, orient_normals=True, cell_centers=None):
    """

    Parameters
    ----------
    all_wall_topomesh
    property_name
    orient_normals

    Returns
    -------

    """

    if property_name in ['area']:
        compute_topomesh_property(all_wall_topomesh,property_name,2)
        compute_topomesh_cell_property_from_faces(all_wall_topomesh,property_name,reduce='sum',weighting='uniform')

    elif property_name in ['barycenter']:
        compute_topomesh_property(all_wall_topomesh,property_name,2)
        compute_topomesh_cell_property_from_faces(all_wall_topomesh,property_name,reduce='mean',weighting='area')

    elif property_name in ['normal']:

        logging.info("    --> Computing normal 2")
        compute_topomesh_property(all_wall_topomesh,property_name,2,normal_method='orientation')
        logging.info("    --> Computed normal 2")
        compute_topomesh_cell_property_from_faces(all_wall_topomesh,property_name,reduce='mean',weighting='area')
        logging.info("    --> Computed normal 3")

        if orient_normals:
            if not all_wall_topomesh.has_wisp_property("barycenter", 3, is_computed=True):
                compute_wall_property(all_wall_topomesh, "barycenter")

            if cell_centers is None:
                cell_centers = estimate_cell_centers(all_wall_topomesh)
            wall_centers = all_wall_topomesh.wisp_property('barycenter',3)

            wall_cells = all_wall_topomesh.wisp_property('cell_labels', 3).values()
            wall_cell_centers = cell_centers.values(wall_cells)
            logging.info("    --> Computed wall cell centers")
            wall_cell_centers[:,0][wall_cells[:,0] == 1] = wall_centers.values(np.array(list(all_wall_topomesh.wisps(3)))[wall_cells[:,0] == 1])
            wall_cell_centers[:,1][wall_cells[:,1] == 1] = wall_centers.values(np.array(list(all_wall_topomesh.wisps(3)))[wall_cells[:,1] == 1])

            normal_left_to_right = np.sign(np.einsum("ij,ij->i",all_wall_topomesh.wisp_property('normal',3).values(),wall_cell_centers[:,1]-wall_cell_centers[:,0]))
            normal_left_to_right = array_dict(dict(zip(all_wall_topomesh.wisps(3),normal_left_to_right)))

            compute_topomesh_property(all_wall_topomesh, "cells", 2)
            face_cells = np.array([c[0] for c in all_wall_topomesh.wisp_property("cells", 2).values(list(all_wall_topomesh.wisps(2)))])
            face_normal_left_to_right = normal_left_to_right.values(face_cells)
            face_vectors = all_wall_topomesh.wisp_property(property_name,2).values(list(all_wall_topomesh.wisps(2)))
            oriented_face_vectors = face_normal_left_to_right[:,np.newaxis]*face_vectors
            all_wall_topomesh.update_wisp_property(property_name,2,dict(zip(all_wall_topomesh.wisps(2),oriented_face_vectors)))

            compute_topomesh_cell_property_from_faces(all_wall_topomesh,property_name,reduce='mean',weighting='area')

        vectors = all_wall_topomesh.wisp_property(property_name,3).values()
        normalized_vectors = vectors/np.linalg.norm(vectors,axis=1)[:,np.newaxis]
        all_wall_topomesh.update_wisp_property(property_name,3,dict(zip(all_wall_topomesh.wisps(3),normalized_vectors)))


def compute_wall_angle(all_wall_topomesh, direction=np.array([0,0,1],float), signed=False):
    """

    Parameters
    ----------
    all_wall_topomesh
    direction
    signed

    Returns
    -------

    """

    if not all_wall_topomesh.has_wisp_property('normal',3,is_computed=True):
        compute_wall_property(all_wall_topomesh,'normal')
    if signed:
        wall_angles = np.degrees(np.arccos(np.dot(all_wall_topomesh.wisp_property('normal',3).values(),direction)))
    else:
        wall_angles = np.degrees(np.arccos(np.abs(np.dot(all_wall_topomesh.wisp_property('normal',3).values(),direction))))

    all_wall_topomesh.update_wisp_property('angle',3,dict(zip(all_wall_topomesh.wisps(3),wall_angles)))


def estimate_cell_centers(all_wall_topomesh):
    """

    Parameters
    ----------
    all_wall_topomesh (:class:`cellcomplex.property_topomesh.PropertyTopomesh`)

    Returns
    -------

    """

    assert all_wall_topomesh.has_wisp_property("cell_labels",3,is_computed=True)

    cell_labels = np.unique(all_wall_topomesh.wisp_property("cell_labels",3).values())

    if not all_wall_topomesh.has_wisp_property("barycenter",3,is_computed=True):
        compute_wall_property(all_wall_topomesh,"barycenter")

    if not all_wall_topomesh.has_wisp_property("area", 3, is_computed=True):
        compute_wall_property(all_wall_topomesh, "area")

    wall_cells = np.concatenate(all_wall_topomesh.wisp_property('cell_labels',3).values())
    wall_cell_walls = np.concatenate([[w for c in all_wall_topomesh.wisp_property('cell_labels',3)[w]] for w in all_wall_topomesh.wisps(3)])

    wall_cell_wall_centers = all_wall_topomesh.wisp_property("barycenter",3).values(wall_cell_walls)
    wall_cell_wall_areas = all_wall_topomesh.wisp_property("area",3).values(wall_cell_walls)

    cell_centers = np.transpose([nd.sum(wall_cell_wall_areas*wall_cell_wall_centers[:,k],wall_cells,index=cell_labels) for k in range(3)])
    cell_centers = cell_centers/nd.sum(wall_cell_wall_areas,wall_cells,index=cell_labels)[:,np.newaxis]

    return array_dict(cell_centers,keys=cell_labels)


def estimate_cell_scalar_property(all_wall_topomesh, property_name):
    """

    Parameters
    ----------
    all_wall_topomesh
    property_name

    Returns
    -------

    """

    assert all_wall_topomesh.has_wisp_property("cell_labels",3,is_computed=True)

    cell_labels = np.unique(all_wall_topomesh.wisp_property("cell_labels",3).values())

    if not all_wall_topomesh.has_wisp_property("area", 3, is_computed=True):
        compute_wall_property(all_wall_topomesh, "area")

    wall_cells = np.concatenate(all_wall_topomesh.wisp_property('cell_labels',3).values())
    wall_cell_walls = np.concatenate([[w for c in all_wall_topomesh.wisp_property('cell_labels',3)[w]] for w in all_wall_topomesh.wisps(3)])

    wall_cell_wall_property = all_wall_topomesh.wisp_property(property_name,3).values(wall_cell_walls)
    wall_cell_wall_areas = all_wall_topomesh.wisp_property("area",3).values(wall_cell_walls)

    cell_property = nd.sum(wall_cell_wall_areas*wall_cell_wall_property,wall_cells,index=cell_labels)
    cell_property = cell_property/nd.sum(wall_cell_wall_areas,wall_cells,index=cell_labels)

    return array_dict(dict(zip(cell_labels,cell_property)))