# -*- python -*-
# -*- coding: utf-8 -*-
#
#       tissue_paredes.wall_extraction
#
#       File author(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File maintainer(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
# ------------------------------------------------------------------------------


import logging
from time import time as current_time
from copy import deepcopy
import os
from multiprocessing.pool import ThreadPool as Pool
# from multiprocessing import Pool
import gc

import numpy as np
import scipy.ndimage as nd

from scipy.cluster.vq import vq

from timagetk.components import SpatialImage
from timagetk.plugins.resampling import isometric_resampling

from cellcomplex.triangular_mesh import TriangularMesh

from cellcomplex.property_topomesh.creation import triangle_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_vertex_property_from_faces, compute_topomesh_cell_property_from_faces
from cellcomplex.property_topomesh.extraction import cell_topomesh
from cellcomplex.property_topomesh.optimization import property_topomesh_vertices_deformation, property_topomesh_isotropic_remeshing
from cellcomplex.property_topomesh.composition import append_topomesh

from cellcomplex.property_topomesh.utils.implicit_surfaces import vtk_marching_cubes
from cellcomplex.property_topomesh.utils.vtk_optimization_tools import property_topomesh_vtk_smoothing_decimation

from tissue_analysis.property_spatial_image import PropertySpatialImage


def extract_wall_meshes(input_seg_img, wall_types=['anticlinal_L1'], resampling_voxelsize = None, microscope_orientation = 1, smoothing = True, target_edge_length = 1.):
    """Extract the anticlinal L1 walls in a segmented image as triangular meshes

    Parameters
    ----------
    input_seg_img
    wall_types
    resampling_voxelsize
    microscope_orientation
    smoothing
    target_edge_length

    Returns
    -------

    """

    seg_img = deepcopy(input_seg_img)
    if resampling_voxelsize is not None:
        start_time = current_time()
        old_shape = seg_img.shape
        seg_img = isometric_resampling(seg_img,method=float(resampling_voxelsize),option='label')
        logging.info("--> Resampling segmented image"+str(old_shape)+" -> "+str(seg_img.shape)+" ["+str(current_time() - start_time)+" s]")

    start_time = current_time()
    p_img = PropertySpatialImage(seg_img,ignore_cells_at_stack_margins=False)
    p_img.compute_default_image_properties()
    if 'all' in wall_types:
        L1_cells = [l for l in p_img.labels if p_img.image_property('layer')[l]==1]
        p_img.update_image_property('layer',dict(zip(p_img.labels,[1 for l in p_img.labels])))
    logging.info("--> Computing image cell layers ["+str(current_time() - start_time)+" s]")

    start_time = current_time()
    anticlinal_walls = np.concatenate([[(l,n) for n in p_img.image_graph.neighbors(l) if p_img.image_property('layer')[n]==1] for l in p_img.labels if p_img.image_property('layer')[l]==1 if len([n for n in p_img.image_graph.neighbors(l) if p_img.image_property('layer')[n]==1])>0])
    if 'all' in wall_types:
        anticlinal_walls = np.concatenate([anticlinal_walls,[(1,l) for l in L1_cells]])
    anticlinal_walls = np.unique(np.sort(anticlinal_walls),axis=0)
    print(anticlinal_walls)
    logging.info("--> Extracting L1 anticlinal walls ["+str(current_time() - start_time)+" s]")

    cell_labels = [l for l in p_img.labels if p_img.image_property('layer')[l]==1]
    cell_images = [seg_img==l for l in p_img.labels if p_img.image_property('layer')[l]==1]

    start_time = current_time()
    pool = Pool()
    cell_topomeshes = pool.map(lambda cell_img : vtk_marching_cubes(cell_img,smoothing=0,decimation=0), cell_images)
    del pool
    gc.collect()
    logging.info("  --> Marching cubes on cells "+str(cell_labels)+" [" + str(current_time() - start_time) + " s]")

    cell_meshes = dict(zip(cell_labels,cell_topomeshes))

    # cell_meshes = {}
    # for l in p_img.labels:
    #     if p_img.image_property('layer')[l]==1:
    #         start_time = current_time()
    #         cell_meshes[l] = vtk_marching_cubes(seg_img==l,smoothing=0,decimation=0)
    #         logging.info("  --> Marching cubes on cell "+str(l)+" ["+str(current_time() - start_time)+" s]")
    #
    if 'all' in wall_types:
        start_time = current_time()
        cell_meshes[1] = vtk_marching_cubes(seg_img==1,smoothing=0,decimation=0)
        logging.info("  --> Marching cubes on background ["+str(current_time() - start_time)+" s]")

    def wall_mesh(cell_meshes, left_label, right_label, voxelsize = seg_img.voxelsize, smoothing=smoothing, target_edge_length=target_edge_length):

        start_time = current_time()
        left_points, left_triangles = cell_meshes[left_label]
        right_points, right_triangles = cell_meshes[right_label]

        left_mesh = TriangularMesh()
        left_mesh.points = dict(zip(np.arange(len(left_points)), microscope_orientation * left_points * np.array(seg_img.voxelsize)))
        left_mesh.triangles = dict(zip(np.arange(len(left_triangles)), left_triangles))
        left_mesh.triangle_data = dict(zip(np.arange(len(left_triangles)), [left_label for t in range(len(left_triangles))]))
        # world.add(left_mesh,"left_mesh",colormap='glasbey')

        right_mesh = TriangularMesh()
        right_mesh.points = dict(zip(np.arange(len(right_points)), microscope_orientation * right_points * np.array(seg_img.voxelsize)))
        right_mesh.triangles = dict(zip(np.arange(len(right_triangles)), right_triangles))
        right_mesh.triangle_data = dict(zip(np.arange(len(right_triangles)), [right_label for t in range(len(right_triangles))]))
        # world.add(right_mesh,"right_mesh",colormap='glasbey')

        right_left_matching = vq(right_points * np.array(seg_img.voxelsize), left_points * np.array(seg_img.voxelsize))

        # right_mesh.point_data = dict(zip(np.arange(len(right_points)),np.isclose(right_left_matching[1],0,atol=0.1)))
        # world.add(right_mesh,"right_mesh",colormap='glasbey')

        matching_vertices = np.isclose(right_left_matching[1], 0, atol=0.1)

        if matching_vertices.sum() > 0:

            right_wall_vertices = np.arange(len(right_points))[matching_vertices]
            right_wall_triangles = right_triangles[np.all([[v in right_wall_vertices for v in t] for t in right_triangles], axis=1)]

            if len(right_wall_triangles) > 0:
                wall_topomesh = triangle_topomesh(right_wall_triangles, dict(zip(right_wall_vertices, (microscope_orientation * right_points * np.array(seg_img.voxelsize))[right_wall_vertices])))
                # wall_topomesh.update_wisp_property("wall_label", 0, np.array([i_w + 2 for v in wall_topomesh.wisps(0)]))
                # wall_label = wall_topomesh.wisp_property('wall_label', 0).values().mean()

                if smoothing:
                    smooth_wall_topomesh = deepcopy(wall_topomesh)
                    smooth_wall_topomesh = property_topomesh_isotropic_remeshing(smooth_wall_topomesh, maximal_length=np.min(voxelsize), iterations=np.ceil(np.log2(np.max(voxelsize) / np.min(voxelsize))))

                    # property_topomesh_vertices_deformation(smooth_wall_topomesh,omega_forces={'taubin_smothing':0.65},iterations=10,gaussian_sigma=0.5)
                    compute_topomesh_property(smooth_wall_topomesh, 'area', 2)
                    triangle_area = smooth_wall_topomesh.wisp_property('area', 2).values().sum() / float(smooth_wall_topomesh.nb_wisps(2))
                    decimation = np.ceil((np.power(target_edge_length, 2) * np.sqrt(2.) / 4.) / triangle_area)
                    # print "Before decimation : ",smooth_wall_topomesh.nb_wisps(0)," Vertices, ",smooth_wall_topomesh.nb_wisps(2)," Triangles"
                    smooth_wall_topomesh = property_topomesh_vtk_smoothing_decimation(smooth_wall_topomesh, smoothing=0, decimation=decimation)
                    # print "After decimation  : ",smooth_wall_topomesh.nb_wisps(0)," Vertices, ",smooth_wall_topomesh.nb_wisps(2)," Triangles"

                    if (smooth_wall_topomesh is not None) and (smooth_wall_topomesh.nb_wisps(2) > 5):
                        smooth_wall_topomesh = property_topomesh_isotropic_remeshing(smooth_wall_topomesh, maximal_length=target_edge_length, iterations=5)
                        # smooth_wall_topomesh.update_wisp_property("wall_label", 0, np.array([wall_label for v in smooth_wall_topomesh.wisps(0)]))

                        del smooth_wall_topomesh._wisp_properties[0]['valency']
                        del smooth_wall_topomesh._wisp_properties[0]['faces']
                        del smooth_wall_topomesh._wisp_properties[1]['borders']
                        del smooth_wall_topomesh._wisp_properties[1]['faces']
                        del smooth_wall_topomesh._wisp_properties[1]['vertices']
                        del smooth_wall_topomesh._wisp_properties[2]['borders']
                        del smooth_wall_topomesh._wisp_properties[2]['vertices']

                        logging.getLogger().setLevel(logging.INFO)
                        logging.info("    --> Vertex properties : "+str(list(smooth_wall_topomesh.wisp_property_names(0))))
                        logging.info("    --> Edge properties : "+str(list(smooth_wall_topomesh.wisp_property_names(1))))
                        logging.info("    --> Triangle properties : "+str(list(smooth_wall_topomesh.wisp_property_names(2))))
                        logging.info("    --> Wall properties : "+str(list(smooth_wall_topomesh.wisp_property_names(3))))

                    return smooth_wall_topomesh
                else:
                    return wall_topomesh

    start_time = current_time()
    pool = Pool()
    wall_topomeshes = pool.map(lambda labels: wall_mesh(cell_meshes, labels[0], labels[1]), anticlinal_walls)
    del pool
    gc.collect()

    logging.info("  --> Smoothing "+ str(len(wall_topomeshes)) + " wall meshes [" + str(current_time() - start_time) + " s]")

    wall_meshes = dict([((l,r),mesh) for (l,r),mesh in zip(anticlinal_walls, wall_topomeshes) if (mesh is not None) and (mesh.nb_wisps(2) > 5)])

    # wall_meshes = {}
    # for i_w, (left_label, right_label) in enumerate(anticlinal_walls):
    #     start_time = current_time()
    #     left_points, left_triangles = cell_meshes[left_label]
    #     right_points, right_triangles = cell_meshes[right_label]
    # 
    #     left_mesh = TriangularMesh()
    #     left_mesh.points = dict(zip(np.arange(len(left_points)),microscope_orientation*left_points*np.array(seg_img.voxelsize)))
    #     left_mesh.triangles = dict(zip(np.arange(len(left_triangles)),left_triangles))
    #     left_mesh.triangle_data = dict(zip(np.arange(len(left_triangles)),[left_label for t in range(len(left_triangles))]))
    #     # world.add(left_mesh,"left_mesh",colormap='glasbey')
    # 
    #     right_mesh = TriangularMesh()
    #     right_mesh.points = dict(zip(np.arange(len(right_points)),microscope_orientation*right_points*np.array(seg_img.voxelsize)))
    #     right_mesh.triangles = dict(zip(np.arange(len(right_triangles)),right_triangles))
    #     right_mesh.triangle_data = dict(zip(np.arange(len(right_triangles)),[right_label for t in range(len(right_triangles))]))
    #     # world.add(right_mesh,"right_mesh",colormap='glasbey')
    # 
    #     right_left_matching = vq(right_points*np.array(seg_img.voxelsize),left_points*np.array(seg_img.voxelsize))
    # 
    #     # right_mesh.point_data = dict(zip(np.arange(len(right_points)),np.isclose(right_left_matching[1],0,atol=0.1)))
    #     # world.add(right_mesh,"right_mesh",colormap='glasbey')
    # 
    #     matching_vertices = np.isclose(right_left_matching[1],0,atol=0.1)
    # 
    #     if matching_vertices.sum()>0:
    # 
    #         right_wall_vertices = np.arange(len(right_points))[matching_vertices]
    #         right_wall_triangles = right_triangles[np.all([[v in right_wall_vertices for v in t] for t in right_triangles],axis=1)]
    # 
    #         if len(right_wall_triangles)>0:
    #             wall_topomesh = triangle_topomesh(right_wall_triangles,dict(zip(right_wall_vertices,(microscope_orientation*right_points*np.array(seg_img.voxelsize))[right_wall_vertices])))
    #             wall_topomesh.update_wisp_property("wall_label",0,np.array([i_w+2 for v in wall_topomesh.wisps(0)]))
    # 
    #             logging.getLogger().setLevel(logging.INFO)
    #             logging.info("    --> Vertex properties : " + str(list(wall_topomesh.wisp_property_names(0))))
    #             logging.info("    --> Edge properties : " + str(list(wall_topomesh.wisp_property_names(1))))
    #             logging.info("    --> Triangle properties : " + str(list(wall_topomesh.wisp_property_names(2))))
    #             logging.info("    --> Wall properties : " + str(list(wall_topomesh.wisp_property_names(3))))
    # 
    #             wall_meshes[(left_label,right_label)] = wall_topomesh
    # 
    #             logging.info("  --> Matching wall mesh ("+str(left_label)+","+str(right_label)+") ["+str(current_time() - start_time)+" s]")

    wall_cells = {}

    meshed_anticlinal_walls = [(l,r) for (l,r) in anticlinal_walls if (l,r) in wall_meshes.keys()]

    all_wall_topomesh = deepcopy(wall_meshes[meshed_anticlinal_walls[0]])
    wall_label = 1
    all_wall_topomesh.update_wisp_property("wall_label", 0, np.array([wall_label for v in all_wall_topomesh.wisps(0)]))
    wall_cells[wall_label] = tuple(list(anticlinal_walls[0]))
    for i_w, (left_label, right_label) in enumerate(meshed_anticlinal_walls[1:]):
        wall_topomesh = wall_meshes[(left_label, right_label)]
        wall_label = i_w+2
        #wall_label = np.arange(len(anticlinal_walls))[np.all(np.array(anticlinal_walls) == np.array((left_label, right_label)), axis=1)][0] + 1
        wall_topomesh.update_wisp_property("wall_label", 0, np.array([wall_label for v in wall_topomesh.wisps(0)]))
        start_time = current_time()
        logging.info("  --> Appending smooth wall mesh " + str(i_w + 1) + "/" + str(len(anticlinal_walls)))
        wall_cells[wall_label] = (left_label, right_label)
        all_wall_topomesh, _ = append_topomesh(all_wall_topomesh, wall_topomesh, properties_to_append={0: ['wall_label'], 1: [], 2: [], 3: []})
        logging.info("  <-- Appending smooth wall mesh  (" + str(left_label) + "," + str(right_label) + ") [" + str(current_time() - start_time) + " s]")
    all_wall_topomesh.update_wisp_property('cell_labels', 3, dict(zip(list(all_wall_topomesh.wisps(3)), [wall_cells[int(all_wall_topomesh.wisp_property("wall_label", 0)[list(all_wall_topomesh.borders(3, c, 3))[0]])] for c in all_wall_topomesh.wisps(3)])))

    return all_wall_topomesh


# def smooth_wall_meshes(all_wall_topomesh, seg_img, target_edge_length=1.):
#     """
#
#     Parameters
#     ----------
#     all_wall_topomesh
#     seg_img
#     target_edge_length
#
#     Returns
#     -------
#
#     """
#
#     anticlinal_walls = all_wall_topomesh.wisp_property('cell_labels', 3).values()
#
#     wall_meshes = {}
#     for c in all_wall_topomesh.wisps(3):
#         wall_topomesh = cell_topomesh(all_wall_topomesh, cells=[c], copy_properties=True, preserve_ids=False)
#         wall_meshes[tuple(wall_topomesh.wisp_property('cell_labels', 3).values()[0])] = wall_topomesh
#
#     wall_cells = {}
#     for i_w, (left_label, right_label) in enumerate(anticlinal_walls):
#         if (left_label, right_label) in wall_meshes:
#             wall_cells[i_w + 2] = (left_label, right_label)
#
#     logging.getLogger().setLevel(logging.INFO)
#
#     wall_topomeshes = [wall_meshes[(left_label, right_label)] for i_w, (left_label, right_label) in enumerate(wall_meshes.keys())]
#     wall_cell_labels = [(left_label, right_label) for i_w, (left_label, right_label) in enumerate(wall_meshes.keys())]
#
#     def smooth_wall_mesh(wall_topomesh, voxelsize=seg_img.voxelsize, target_edge_length=target_edge_length):
#         wall_label = wall_topomesh.wisp_property('wall_label', 0).values().mean()
#         smooth_wall_topomesh = deepcopy(wall_topomesh)
#         smooth_wall_topomesh = property_topomesh_isotropic_remeshing(smooth_wall_topomesh, maximal_length=np.min(voxelsize),
#                                                                      iterations=np.ceil(np.log2(np.max(voxelsize) / np.min(voxelsize))))
#         # property_topomesh_vertices_deformation(smooth_wall_topomesh,omega_forces={'taubin_smothing':0.65},iterations=10,gaussian_sigma=0.5)
#         compute_topomesh_property(smooth_wall_topomesh, 'area', 2)
#         triangle_area = smooth_wall_topomesh.wisp_property('area', 2).values().sum() / float(smooth_wall_topomesh.nb_wisps(2))
#         decimation = np.ceil((np.power(target_edge_length, 2) * np.sqrt(2.) / 4.) / triangle_area)
#         # print "Before decimation : ",smooth_wall_topomesh.nb_wisps(0)," Vertices, ",smooth_wall_topomesh.nb_wisps(2)," Triangles"
#         smooth_wall_topomesh = property_topomesh_vtk_smoothing_decimation(smooth_wall_topomesh, smoothing=0, decimation=decimation)
#         # print "After decimation  : ",smooth_wall_topomesh.nb_wisps(0)," Vertices, ",smooth_wall_topomesh.nb_wisps(2)," Triangles"
#
#         if (smooth_wall_topomesh is not None) and (smooth_wall_topomesh.nb_wisps(2) > 5):
#             smooth_wall_topomesh = property_topomesh_isotropic_remeshing(smooth_wall_topomesh, maximal_length=target_edge_length, iterations=5)
#             smooth_wall_topomesh.update_wisp_property("wall_label", 0, np.array([wall_label for v in smooth_wall_topomesh.wisps(0)]))
#
#             del smooth_wall_topomesh._wisp_properties[0]['valency']
#             del smooth_wall_topomesh._wisp_properties[0]['faces']
#             del smooth_wall_topomesh._wisp_properties[1]['borders']
#             del smooth_wall_topomesh._wisp_properties[1]['faces']
#             del smooth_wall_topomesh._wisp_properties[1]['vertices']
#             del smooth_wall_topomesh._wisp_properties[2]['borders']
#             del smooth_wall_topomesh._wisp_properties[2]['vertices']
#
#             logging.getLogger().setLevel(logging.INFO)
#             logging.info("    --> Vertex properties : "+str(list(smooth_wall_topomesh.wisp_property_names(0))))
#             logging.info("    --> Edge properties : "+str(list(smooth_wall_topomesh.wisp_property_names(1))))
#             logging.info("    --> Triangle properties : "+str(list(smooth_wall_topomesh.wisp_property_names(2))))
#             logging.info("    --> Wall properties : "+str(list(smooth_wall_topomesh.wisp_property_names(3))))
#
#         return smooth_wall_topomesh
#
#     start_time = current_time()
#     pool = Pool()
#     smooth_wall_topomeshes = pool.map(smooth_wall_mesh, wall_topomeshes)
#     del pool
#     gc.collect()
#
#     logging.info("  --> Smoothing "+ str(len(wall_meshes)) + " wall meshes [" + str(current_time() - start_time) + " s]")
#
#     smoothed_wall_meshes = dict([((l,r),mesh) for (l,r),mesh in zip(wall_cell_labels, smooth_wall_topomeshes) if (mesh is not None) and (mesh.nb_wisps(2) > 5)])
#
#     # for i_w, (left_label, right_label) in enumerate(wall_meshes.keys()):
#     #     start_time = current_time()
#     #     wall_topomesh = wall_meshes[(left_label, right_label)]
#     #     wall_label = wall_topomesh.wisp_property('wall_label', 0).values().mean()
#     #     smooth_wall_topomesh = deepcopy(wall_topomesh)
#     #     smooth_wall_topomesh = property_topomesh_isotropic_remeshing(smooth_wall_topomesh, maximal_length=np.min(seg_img.voxelsize),
#     #                                                                  iterations=np.ceil(np.log2(np.max(seg_img.voxelsize) / np.min(seg_img.voxelsize))))
#     #     # property_topomesh_vertices_deformation(smooth_wall_topomesh,omega_forces={'taubin_smothing':0.65},iterations=10,gaussian_sigma=0.5)
#     #     compute_topomesh_property(smooth_wall_topomesh, 'area', 2)
#     #     triangle_area = smooth_wall_topomesh.wisp_property('area', 2).values().sum() / float(smooth_wall_topomesh.nb_wisps(2))
#     #     decimation = np.ceil((np.power(target_edge_length, 2) * np.sqrt(2.) / 4.) / triangle_area)
#     #     # print "Before decimation : ",smooth_wall_topomesh.nb_wisps(0)," Vertices, ",smooth_wall_topomesh.nb_wisps(2)," Triangles"
#     #     smooth_wall_topomesh = property_topomesh_vtk_smoothing_decimation(smooth_wall_topomesh, smoothing=0, decimation=decimation)
#     #     # print "After decimation  : ",smooth_wall_topomesh.nb_wisps(0)," Vertices, ",smooth_wall_topomesh.nb_wisps(2)," Triangles"
#     #
#     #     if (smooth_wall_topomesh is not None) and (smooth_wall_topomesh.nb_wisps(2) > 5):
#     #         smooth_wall_topomesh = property_topomesh_isotropic_remeshing(smooth_wall_topomesh, maximal_length=target_edge_length, iterations=5)
#     #         smooth_wall_topomesh.update_wisp_property("wall_label", 0, np.array([wall_label for v in smooth_wall_topomesh.wisps(0)]))
#     #         smoothed_wall_meshes[(left_label, right_label)] = smooth_wall_topomesh
#     #         # print wall_topomesh.nb_wisps(2)," --> ",smooth_wall_topomesh.nb_wisps(2)," [",decimation,"]"
#     #         # world.add(smooth_wall_topomesh,"smoothed_wall")
#
#     logging.getLogger().setLevel(logging.INFO)
#
#     wall_cells = {}
#     all_smooth_wall_topomesh = deepcopy(smoothed_wall_meshes.values()[0])
#     wall_label = smoothed_wall_meshes.values()[0].wisp_property('wall_label', 0).values()[0]
#     wall_cells[wall_label] = smoothed_wall_meshes.keys()[0]
#     for left_label, right_label in smoothed_wall_meshes.keys()[1:]:
#         start_time = current_time()
#         logging.info("  --> Appending smooth wall mesh " + str(i_w + 1) + "/" + str(len(anticlinal_walls)))
#         wall_label = smoothed_wall_meshes[(left_label, right_label)].wisp_property('wall_label', 0).values()[0]
#         wall_cells[wall_label] = (left_label, right_label)
#         all_smooth_wall_topomesh, _ = append_topomesh(all_smooth_wall_topomesh, smoothed_wall_meshes[(left_label, right_label)], properties_to_append={0: ['wall_label'], 1: [], 2: [], 3: []})
#         logging.info("  <-- Appending smooth wall mesh  (" + str(left_label) + "," + str(right_label) + ") [" + str(current_time() - start_time) + " s]")
#     all_smooth_wall_topomesh.update_wisp_property('cell_labels', 3, dict(zip(list(all_smooth_wall_topomesh.wisps(3)), [
#         wall_cells[int(all_smooth_wall_topomesh.wisp_property("wall_label", 0)[list(all_smooth_wall_topomesh.borders(3, c, 3))[0]])] for c in all_smooth_wall_topomesh.wisps(3)])))
#
#     return all_smooth_wall_topomesh