# -*- python -*-
# -*- coding: utf-8 -*-
#
#       tissue_paredes - test
#
#       File author(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File maintainer(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
# ------------------------------------------------------------------------------


import unittest
import numpy as np

from tissue_paredes.wall_extraction import extract_wall_meshes
from tissue_paredes.wall_analysis import compute_wall_property, compute_wall_angle

from tissue_paredes.utils.tissue_image_tools import square_tissue_image

from tissue_analysis.property_spatial_image import PropertySpatialImage

class TestWallAnalysis(unittest.TestCase):
    """Tests the wall analysis functionalities.
    """

    def setUp(self):
        self.img = square_tissue_image()
        self.wall_topomesh = extract_wall_meshes(self.img ,wall_types=['anticlinal_L1'], resampling_voxelsize=0.5, smoothing=True)
        self.cell_centers = PropertySpatialImage(self.img).image_property("barycenter")

    def tearDown(self):
        pass

    def test_wall_area(self):
        compute_wall_property(self.wall_topomesh,'area')

        assert self.wall_topomesh.has_wisp_property('area',3)
        max_area = (self.img.shape[0]*self.img.voxelsize[0]/2)*(self.img.shape[2]*self.img.voxelsize[2]/2)
        min_area = (self.img.shape[0]*self.img.voxelsize[0]/4)*(self.img.shape[2]*self.img.voxelsize[2]/4)
        print(self.wall_topomesh.wisp_property('area',3).values(),min_area,max_area)
        assert np.all(self.wall_topomesh.wisp_property('area',3).values() < max_area)
        assert np.all(self.wall_topomesh.wisp_property('area',3).values() > min_area)

    def test_wall_center(self):
        compute_wall_property(self.wall_topomesh, 'barycenter')

        assert self.wall_topomesh.has_wisp_property('barycenter', 3)
        layer_center = 3.*(self.img.shape[2]-1)*self.img.voxelsize[2]/4.
        print(self.wall_topomesh.wisp_property('barycenter', 3).values(),layer_center)
        assert np.all(np.isclose(self.wall_topomesh.wisp_property('barycenter', 3).values()[:,2],layer_center,atol=self.img.voxelsize[2]))

    def test_wall_normal(self):
        compute_wall_property(self.wall_topomesh, 'normal', orient_normals=False)

        assert self.wall_topomesh.has_wisp_property('normal', 3)
        assert np.all(np.isclose(self.wall_topomesh.wisp_property('normal', 3).values()[:, 2], 0, atol=0.01))

    def test_wall_angle(self):
        compute_wall_angle(self.wall_topomesh, np.array([0,0,1]))

        assert self.wall_topomesh.has_wisp_property('angle', 3)
        assert np.all(np.isclose(self.wall_topomesh.wisp_property('angle', 3).values(), 90, atol=0.1))

    def test_wall_oriented_normal(self):
        compute_wall_property(self.wall_topomesh, 'normal', orient_normals=True)

        wall_cells = self.wall_topomesh.wisp_property('cell_labels', 3).values()
        wall_cell_centers = self.cell_centers.values(wall_cells)
        normal_left_to_right = np.sign(np.einsum("ij,ij->i", self.wall_topomesh.wisp_property('normal', 3).values(), wall_cell_centers[:, 1] - wall_cell_centers[:, 0]))

        assert self.wall_topomesh.has_wisp_property('normal', 3)
        assert np.all(normal_left_to_right==1)
