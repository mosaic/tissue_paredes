# -*- python -*-
# -*- coding: utf-8 -*-
#
#       tissue_paredes - test
#
#       File author(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File maintainer(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
# ------------------------------------------------------------------------------


import unittest
import numpy as np

from tissue_paredes.wall_extraction import extract_wall_meshes
from tissue_paredes.wall_signal_quantification import quantify_wall_signals, quantify_wall_membrane_signals

from tissue_paredes.utils.tissue_image_tools import square_tissue_image, pseudo_gradient_norm

class TestWallSignalQuantification(unittest.TestCase):
    """Tests the wall signal quantification functionalities.
    """

    def setUp(self):
        self.img = square_tissue_image(size=20)

        self.intensity = 100
        self.wall_sigma = 0.6
        self.wall_intensity = np.pi*self.intensity*np.power((1./(np.sqrt(2*np.pi)*self.wall_sigma)),3)
        
        signal_img = pseudo_gradient_norm(self.img,wall_sigma=self.wall_sigma,intensity=self.intensity)
        self.img_dict = dict([('signal',signal_img)])

        self.all_wall_topomesh = extract_wall_meshes(self.img ,wall_types=['anticlinal_L1'], resampling_voxelsize=0.25, smoothing=True, target_edge_length=3.)

    def tearDown(self):
        pass

    def test_wall_signals(self):
        quantify_wall_signals(self.all_wall_topomesh,self.img_dict,wall_sigma=0.6)
        assert self.all_wall_topomesh.has_wisp_property('signal',3,is_computed=True)
        signal_values = self.all_wall_topomesh.wisp_property('signal',3).values()
        signal_values = signal_values[np.logical_not(np.isnan(signal_values))]
        assert np.all(np.isclose(signal_values,self.intensity,rtol=0.25))

    def test_membrane_quantification(self):
        quantify_wall_membrane_signals(self.all_wall_topomesh,self.img_dict,membrane_channel='signal',channel_names=['signal'])
        assert self.all_wall_topomesh.has_wisp_property('signal',3,is_computed=True)
        signal_values = self.all_wall_topomesh.wisp_property('signal',3).values()
        signal_values = signal_values[np.logical_not(np.isnan(signal_values))]
        assert np.all(np.isclose(signal_values,self.intensity,rtol=0.1))


if  __name__ == '__main__':
    img = square_tissue_image()
    intensity = 100
    wall_sigma = 0.6
    wall_intensity = intensity* np.power((1. / (np.sqrt(2 * np.pi) * wall_sigma)), 3)
    signal_img = pseudo_gradient_norm(img, wall_sigma=wall_sigma, intensity=intensity)
    img_dict = dict([('signal', signal_img)])
    all_wall_topomesh = extract_wall_meshes(img, wall_types=['anticlinal_L1'], resampling_voxelsize=0.5, smoothing=True, target_edge_length=3.)
    quantify_wall_membrane_signals(all_wall_topomesh, img_dict, membrane_channel='signal', channel_names=['signal'])
