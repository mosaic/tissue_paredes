# -*- python -*-
# -*- coding: utf-8 -*-
#
#       tissue_paredes - test
#
#       File author(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File maintainer(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
# ------------------------------------------------------------------------------


import unittest

from tissue_paredes.wall_extraction import extract_wall_meshes

from tissue_paredes.utils.tissue_image_tools import square_tissue_image

class TestWallExtraction(unittest.TestCase):
    """Tests the wall extraction functionalities.
    """

    def setUp(self):
        self.img = square_tissue_image()

    def tearDown(self):
        pass

    def test_wall_extraction(self):
        wall_topomesh = extract_wall_meshes(self.img ,wall_types=['anticlinal_L1'], resampling_voxelsize=0.5, smoothing=False)

        assert wall_topomesh.nb_wisps(3) == 5

    def test_all_wall_extraction(self):
        wall_topomesh = extract_wall_meshes(self.img, wall_types=['all'], resampling_voxelsize=1, smoothing=False)

        assert wall_topomesh.nb_wisps(3) == 9

    def test_smooth_wall_extraction(self):
        wall_topomesh = extract_wall_meshes(self.img ,wall_types=['anticlinal_L1'], resampling_voxelsize=0.5, smoothing=True)

        assert wall_topomesh.nb_wisps(3) == 5